版本
v4.2.0



CodeIgniter 4 is the latest version of the framework, intended for use with PHP 7.3+ (including 8.1).

The initial release was February 24, 2020. The current version is v4.2.0.

You *could* download this version of the framework using the button below, but we encourage you to check the Installation section of the User Guide, to choose from several different ways you can install the framework :)