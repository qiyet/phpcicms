<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register a New User</title>
</head>
<body>
    <?php
        if(isset($message) && !empty($message)){
            echo '<div>'.$message.'</div>';
        }

        echo \Config\Services::validation()->listErrors();

        echo form_open('user/checkuser');
        echo 'Enter your password ', form_input('password','',''), '<br>';
        echo 'Enter your email ', form_input('email','',''), '<br>';
        echo form_submit('','Login');

        echo form_close();
    ?>
    
</body>
</html>
