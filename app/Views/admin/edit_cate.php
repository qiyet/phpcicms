<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>后台首页</title>
</head>
<body>
   这个是后台的首页<br>

   <?= anchor('admin/admin', '首页', 'title="后台首页"');?> <br>
   <?= anchor('admin/login', '登录', 'title="管理登录"');?> | <?= anchor('admin/login/login_out', '退出', 'title="退出登录"');?> <br><br>
 
   -------- 编辑栏目-------<br>
 <form action="<?php echo site_url('admin/category/edit') ?>" method="POST">
	<table class="table">
		<tr>
			<td class="th" colspan="10">编辑栏目</td>
		</tr>
		<tr>
			<td>栏目名称</td>
			<td><input type="text" name="cname" value="<?php echo $category['name'] ?>"/></td>
		</tr>
		<tr>
		<input type="hidden" name="cid" value="<?php echo $category['id'] ?>"/>
			<td colspan="10"><input type="submit" value="编辑" class="input_button"/></td>
		</tr>
	</table>
	</form>

</body>
</html>