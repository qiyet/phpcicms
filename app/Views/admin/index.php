<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>后台首页</title>
</head>
<body>
   这个是后台的首页<br>
   用户账户：<?php $session = \Config\Services::session(); echo $session->get('username');?>
   <br>

   <?= anchor('admin/admin', '首页', 'title="后台首页"');?> <br>
   <?= anchor('admin/login', '登录', 'title="管理登录"');?> | <?= anchor('admin/login/login_out', '退出', 'title="退出登录"');?> <br><br>
   --------  文章管理-------<br>
 
   <?= anchor('admin/article/index', '查看文章', 'title="文章首页"');?> <br>
   <?= anchor('admin/article/add', '添加文章', 'title="添加文章"');?> <br>

   --------  分类管理-------<br>
 
 <?= anchor('admin/category/index', '查看分类', 'title="分类首页"');?> <br>
 <?= anchor('admin/category/addcate', '添加分类', 'title="添加分类"');?> <br>

 --------  单页管理-------<br>
 
 <?= anchor('admin/sgpage/index', '查看单页列表', 'title="单页首页"');?> <br>
 <?= anchor('admin/sgpage/add', '添加单页文章', 'title="添加单页，用于公司介绍等特殊页面"');?> <br>


 ----------系统信息----<br>
 <!-- 右侧 -->
 <div id="right">
			<iframe  frameboder="0" border="0" 	scrolling="yes" name="iframe" src="<?php echo site_url() . '/admin/admin/copy' ?>"></iframe>
		</div>

</body>
</html>