<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>后台首页</title>
</head>
<body>
   这个是后台的首页<br>

   <?= anchor('admin/admin', '首页', 'title="后台首页"');?> <br>
   <?= anchor('admin/login', '登录', 'title="管理登录"');?> | <?= anchor('admin/login/login_out', '退出', 'title="退出登录"');?> <br><br>
 
   -------- 查看文章列表-------<br>
 
 <table class="table">
		<tr>
			<td class="th" colspan="10">查看文章</td>
		</tr>

		<tr>
			<td>AID</td>
			<td>标题</td>
			<td>栏目</td>
			<td>发表时间</td>
			<td>操作</td>
		</tr>
		
		<?php foreach($article as $v): ?>
		<tr>
			<td><?php echo $v['id'] ?></td>
			<td><?php echo $v['title'] ?></td>
			<td><?php echo $v['cid'] ?></td>
			<td><?php //echo date('m-d',$v['time']) ?></td>
			
			<td>
				[<a href="">编辑</a>]
				[<a href="">删除</a>]
			</td>
		</tr>
		<?php endforeach ?>
	</table>
	<div class="page">
		<?= $pager->links() ?>
	</div>


</body>
</html>