<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>后台版权</title>
</head>
<body>
   这个是后台的系统版权页<br>

   <?= anchor('admin/admin', '首页', 'title="后台首页"');?> <br>
   <?= anchor('admin/login', '登录', 'title="管理登录"');?> | <?= anchor('admin/login/login_out', '退出', 'title="退出登录"');?> <br><br>
 
   -------- 查看系统信息-------<br>
   <table class="table">
		<tr>
			<td colspan='2' class="th"><span class="span_people">&nbsp</span>欢迎光临管理后台</td>
		</tr>
		<tr>
			<td>用户名</td>
			<td><?php //echo $this->session->userdata('username') ?></td>
		</tr>
		<tr>
			<td>登录时间</td>
			<td><?php //echo date('m-d',$this->session->userdata('logintime')) ?></td>
		</tr>
		<tr>
			<td>客户端IP</td>
			<td><?php // echo $this->input->ip_address() ?></td>
		</tr>
		<tr>
			<td colspan='2' class="th"><span class="span_server" style="float:left">&nbsp</span>服务器信息</td>
		</tr>
		<tr>
			<td>服务器环境</td>
			<td><?php //echo $this->input->server('SERVER_SOFTWARE') ?></td>
		</tr>
		<tr>
			<td>PHP版本</td>
			<td><?php echo PHP_VERSION ?></td>
		</tr>
		<tr>
			<td>服务器IP</td>
			<td><?php //echo $this->input->server('SERVER_ADDR') ?></td>
		</tr>
		<tr>
			<td>数据库信息</td>
			<td><? //echo $this->db->version(); ?></td>

		</tr>

</table>


</body>
</html>