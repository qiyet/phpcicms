<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>后台首页</title>
</head>
<body>
   这个是后台的首页<br>

   <?= anchor('admin/admin', '首页', 'title="后台首页"');?> <br>
   <?= anchor('admin/login', '登录', 'title="管理登录"');?> | <?= anchor('admin/login/login_out', '退出', 'title="退出登录"');?> <br><br>
 
   -------- 查看栏目列表-------<br>
 
 <?php //var_dump($category);?>
 <table class="table">
		<tr>
			<td class="th" colspan="10">查看栏目</td>
		</tr>
		<tr>
			<td>CID</td>
			<td>栏目名称</td>
			<td>操作</td>
		</tr>

		<?php foreach($category as $v): ?>
		<tr>
			<td><?php echo $v['id'] ?></td>
			<td><?php echo $v['name'] ?></td>
			<td>
				[<a href="<?php echo site_url('admin/category/edit_cate/' . $v['id']) ?>">编辑</a>]

				[<a href="<?php echo site_url('admin/category/del/' . $v['id']) ?>">删除</a>]
			</td>
		</tr>
		<?php endforeach ?>
	</table>

</body>
</html>