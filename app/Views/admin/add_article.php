<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>后台首页</title>

	
<!-- include libraries(jQuery, bootstrap) -->
<link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<!-- include summernote css/js -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
</head>
<body>
   这个是后台的首页<br>

   <?= anchor('admin/admin', '首页', 'title="后台首页"');?> <br>
   <?= anchor('admin/login', '登录', 'title="管理登录"');?> | <?= anchor('admin/login/login_out', '退出', 'title="退出登录"');?> <br><br>
 
   -------- 添加文章列表-------<br>
   <form action="<?php echo site_url('admin/article/send') ?>" method="POST" enctype="multipart/form-data">
	<table class="table">
		<tr >
			<td class="th" colspan="10">发表文章</td>
		</tr>
		<tr>
			<td>标题</td>
			<td><?php echo form_input('title', '填入标题', ['placeholder' => '文章标题...'], 'text'); ?>
			

			</td>
		</tr>
		<tr>
			<td>类型</td>
			<td>
				<input type="radio" name="type" value="0"<?php form_radio('type', '0', TRUE) ?>/> 普通
				<input type="radio" name="type" value="1"<?php form_radio('type', '1') ?>/> 热门
			</td>
		</tr>
		<tr>
			<td>栏目</td>
			<td>
				<select name="cid" id="">
					<?php foreach($category as $v): ?>
					<option value="<?php echo $v['id'] ?>"<?php echo set_select('cid', $v['id']) ?>><?php echo $v['name'] ?></option>
					<?php endforeach ?>
				</select>
			</td>
		</tr>
		<tr>
			<td>缩略图</td>
			<td>
				<input type="file" name="thumb"/>
			</td>
		</tr>
		<tr>
			<td>摘要</td>
			<td>
				<textarea name="info" id="" style="width:550px;height:160px;"><?php echo set_value('info') ?></textarea>
			</td>
		</tr>
		<tr>
			<td>内容</td>
			<td>
				<textarea name="content" id="summernote" style="width:550px;height:500px;"><?php echo set_value('content') ?></textarea>
				
			</td>
		</tr>
		<tr>
			<td colspan="10"><input type="submit" class="input_button" value="发布"/></td>
		</tr>
	</table>
	</form>
	<script>
  let summer = $('#summernote');
summer.summernote({
    callbacks: {
        onImageUpload: function (files) {
            sendFile(files);
        }
    }
});

/** * 发送图片文件给服务器端 */
function sendFile(files) {
    let imageData = new FormData();
    imageData.append("imageData", files[0]);
    $.ajax({
        url: '<?php echo base_url()?>/index.php/test/uploads', // 图片上传url
        type: 'POST',
        data: imageData,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',     // 以json的形式接收返回的数据
        // 图片上传成功
        success: function ($result) {
            let imgNode = document.createElement("img");
            imgNode.src = '/'+$result.img;
            //console.log(imgNode);
            summer.summernote('insertNode', imgNode);
        },
        // 图片上传失败
        error: function () {
            console.log('图片上传失败');
        }
    });
}
</script>
</body>
</html>