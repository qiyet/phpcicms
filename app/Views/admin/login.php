<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>登录</title>
</head>
<body>
    <?php
        if(isset($message) && !empty($message)){
            echo '<div>'.$message.'</div>';
        }

        echo \Config\Services::validation()->listErrors();

        echo form_open('admin/login/checkuser');
        echo '输入用户名 ', form_input('userName','',''), '<br>';
        echo '密码 ', form_input('userPwd','',''), '<br>';
        echo form_submit('','Login');

        echo form_close();
    ?>
    
</body>
</html>