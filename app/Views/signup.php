<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register a New User</title>
</head>
<body>
    <?php
        echo \Config\Services::validation()->listErrors();

        echo form_open('user/newuser');
        echo 'Enter your name ', form_input('name','',''), '<br>';
        echo 'Enter your password ', form_input('password','',''), '<br>';
        echo 'Enter your email ', form_input('email','',''), '<br>';
        echo form_submit('','Create Now');

        echo form_close();
    ?>
    
</body>
</html>
