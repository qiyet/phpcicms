<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>前台首页</title>
</head>
<body>
<div id="header">
		<div class='logo'>
			<a href=""><img src="<?php echo base_url() . 'style/index/' ?>images/logo.jpg" alt=""></a>
		</div>
		<div class='navigation'>
			<a href="<?php echo site_url() .  'index.html'?>">首页</a>
			<?php foreach($category as $v): ?>
			<a href="<?php echo site_url('c/' . $v['id'])?>"><?php echo $v['name'] ?></a>
			<?php endforeach ?>
		</div>
	</div>
</body>
</html>