<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>前台首页</title>
</head>
<body>
<div id="main">
		<div class='news'>

			<?php foreach($article as $v): ?>
			<div class='newsList'>
				<div class='newsImage'>
					<a href="<?php echo site_url('a/' . $v['id']) ?>"><img src="<?php echo base_url() . 'uploads/' . $v['image'] ?>"/></a>
				</div>
				<div class='newsContent'>
					<h3><a href="<?php echo site_url('a/' . $v['id']) ?>"><?php echo $v['title'] ?></a></h3>
					<p><?php echo $v['description'] ?></p>
					<a href="<?php echo site_url('a/' . $v['id']) ?>" class='more'>更多>></a>
				</div>
			</div>
			<?php endforeach ?>

		</div>
        <?= $this->include('index/right') ?>
	
	</div>

</body>
</html>