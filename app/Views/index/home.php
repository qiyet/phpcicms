<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>前台首页</title>
</head>
<body>
   这个是首页<br>
   头部：<?= $this->include('index/head') ?>
   用户账户：<?php $session = \Config\Services::session(); echo $session->get('username');?>

   <div id="main">
		<div class='content'>
			<div  class='list'>
				<div class='title'>
					<h2>最新文章..</h2>
				</div>
				<ul>
					<?php foreach($art as $v): ?>
					<li>
						<div class='post-image'>
							<span>
								<a href="<?php echo site_url('a/' . $v['id']) ?>"><img width="" src="<?php echo base_url() . 'uploads/'. $v['image']?>" /></a>
							</span>	
						</div>	
						<div class='post-content'>
							<a href="<?php echo site_url('a/' . $v['id']) ?>"><h3><?php echo $v['title'] ?></h3></a>
							<p><?php echo $v['description'] ?></p>
						</div>
					</li>
					<?php endforeach ?>
				</ul>
			</div>
			<div  class='list'>
				<div class='title'>
					<h2>热门文章..</h2>
				</div>
				<ul>
					<?php foreach($hot as $v): ?>
					<li>
						<div class='post-image'>
							<span>
								<a href="<?php echo site_url('a/' . $v['id']) ?>"><img width="" src="<?php echo base_url() . 'uploads/'. $v['image']?>" /></a>
							</span>	
						</div>	
						<div class='post-content'>
							<a href="<?php echo site_url('a/' . $v['id']) ?>"><h3><?php echo $v['title'] ?></h3></a>
							<p><?php echo $v['description'] ?></p>
						</div>
					</li>
					<?php endforeach ?>
	
				</ul>
			</div>
		</div>

	</div>

	<?= $this->include('index/foot') ?>

</body>
</html>