<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>前台首页</title>
</head>
<body>
头部：<?= $this->include('index/head') ?>
 <div id="main">
		<div class='details'>
			<h1><?php echo $article['title'] ?></h1>
			<div class='info'>
				<div class='base'>
					<em>发表于 <?php echo date('H:i:s',$article['create_time']) ?></em>, 分类：<strong><?php echo $article['name'] ?></strong>
				</div>
			</div>
			<div class='content'>
				<?php echo $article['content'] ?>
			</div>
		</div>

		<?= $this->include('index/right') ?>
	</div>
	<?= $this->include('index/foot') ?>
</body>
</html>