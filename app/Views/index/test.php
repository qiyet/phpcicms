<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>test页面</title>
<!-- include libraries(jQuery, bootstrap) -->
<link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<!-- include summernote css/js -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
</head>
<body>
   这个是首页<br>
   头部：<?= $this->include('index/head') ?>
   用户账户：<?php $session = \Config\Services::session(); echo $session->get('username');?>
   <br>

  <textarea id="summernote"></textarea>






	<?= $this->include('index/foot') ?>

<script>
  let summer = $('#summernote');
summer.summernote({
    callbacks: {
        onImageUpload: function (files) {
            sendFile(files);
        }
    }
});

/** * 发送图片文件给服务器端 */
function sendFile(files) {
    let imageData = new FormData();
    imageData.append("imageData", files[0]);
    $.ajax({
        url: '<?php echo base_url()?>/index.php/test/uploads', // 图片上传url
        type: 'POST',
        data: imageData,
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',     // 以json的形式接收返回的数据
        // 图片上传成功
        success: function ($result) {
            let imgNode = document.createElement("img");
            imgNode.src = '/'+$result.img;
            //console.log(imgNode);
            summer.summernote('insertNode', imgNode);
        },
        // 图片上传失败
        error: function () {
            console.log('图片上传失败');
        }
    });
}
</script>

</body>
</html>