<?php

namespace App\Controllers\Admin;
use App\Models\SgpageModel;
class Sgpage extends MyController
{
  /**
	 * 构造函数
	 */
	public function __construct(){
		//parent::__construct();
		
    $this->sgpagemodel = new SgpageModel();
	}
	/**
	 * 查看所有文章
	 */
	public function index(){
		$this->sgpagemodel = new SgpageModel();
		//载入分页类
		$data = [
            'article' =>$this->sgpagemodel->paginate(10),//单页文章
            'pager' =>$this->sgpagemodel->pager
        ];
//var_dump($data);exit;
        echo view('admin/sgpage', $data);
  
	}
/**
	 * 发表文章模板显示,表单输入框
	 */
	public function add(){
		//加载文章分类
		$cate=new \App\Models\CategoryModel();
		$data['category'] = $cate->findAll();
		//var_dump($data['category']);exit;

		helper('form');
		echo view('admin/add_sgpage', $data);
	}

	/**
	 * 发表文章动作
	 */
	public function send(){

		//载入表单验证类
		/*$this->load->library('form_validation');*/
		//设置规则
		//$this->form_validation->set_rules('title', '文章标题', 'required|min_length[5]');
		// $this->form_validation->set_rules('type', '类型', 'required|integer');
		// $this->form_validation->set_rules('cid', '栏目', 'integer');
		// $this->form_validation->set_rules('info', '摘要', 'required|max_length[155]');
		// $this->form_validation->set_rules('content', '内容', 'required|max_length[2000]');
		//执行验证
		/*$status = $this->form_validation->run('article');*/
		$status = $this->validate([
			'avatar' => 'uploaded[thumb]|max_size[thumb,1024]'
            //'userPwd'=>'required',
        ]);

		if($status){
			//文件上传------------------------
			$files = $this->request->getFiles('thumb');
			//$newName = $files['thumb']->getName();
			$newName = $files['thumb']->getRandomName(); 
			//配置

			//$newName=time() . mt_rand(1000,9999);
			//执行上传（移动上传文件）
			$files['thumb']->move(WRITEPATH.'uploads', $newName);
			//var_dump($files);exit;
			
			//调用 isValid() 方法来检查文件是否是通过 HTTP 无误上传的:
			if (! $files['thumb']->isValid())
			{
					echo $files['thumb']->getErrorString().'('.$files['thumb']->getError().')';
			}
		
		   //缩略图-----------------
		}else{$newName='';}
			//加载articlemodel
			$art=new \App\Models\SgpageModel();


			$data = array(
				'title'	=> $this->request->getpost('title'),
				//'keyword'	=> $this->request->getpost('type'),
				'cid'	=> $this->request->getpost('cid'),
				'image'	=> $newName,
				'description' => $this->request->getpost('info'),
				'content'=> $this->request->getpost('content')
				);	
			// p($data);die;
			$art->add($data);
			success('admin/sgpage/index', '单页发表成功');
}
}