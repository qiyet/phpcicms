<?php

namespace App\Controllers\Admin;
use App\Models\CategoryModel;
class Category extends MyController
{
  /**
	 * 构造函数
	 */
	public function __construct(){
		//parent::__construct();
		//$this->load->model('category_model', 'cate');//ci3加载model方法
    $this->catemodel = new CategoryModel();
	}
	/**
	 * 查看栏目
	 */
	public function index(){
		
	//	$data['category'] = $this->cate->check(); //ci3加载model中的函数
  $data['category'] = $this->catemodel->check(); 
  //var_dump($data['category']);exit;
		echo view('admin/cate', $data);
	}
	/**
	 * 添加栏目
	 */
	public function addcate(){
		// $this->output->enable_profiler(TRUE);
		helper('form');
		echo view('admin/addcate');
	}

	/**
	 * 添加动作
	 */
	public function add(){
		$myrequest = \Config\Services::request();
		/*$this->load->library('form_validation');
		$status = $this->form_validation->run('cate');*/
		$status = $this->validate([
            'cname'=>'required',
            //'userPwd'=>'required',
        ]);

		if($status){
			//echo "数据库操作";
			//echo $_POST['abc'];die;
			// var_dump($this->input->post('abc'));die;

			$data = array(
				'name'	=> $myrequest->getVar('cname')
				);

			//$this->cate->add($data);
			$this->catemodel->add($data);// 也可以用$id = $insert->insert($data);返回id
			success('admin/category/index', '添加成功');
		} else {
			helper('form');
			echo view('admin/add_cate');
		}
	}
/**
	 * 编辑
	 */
	public function edit_cate($cid = null){
	
		//echo $cid;die;
		$cate = new CategoryModel();
		$data['category'] = $cate->check_cate($cid);
		//var_dump($data['category']);exit;

		helper('form');
		echo view('admin/edit_cate', $data);
	}


	/**
	 * 编辑动作
	 */
	public function edit(){
		$status = $this->validate([
            'cname'=>'required',
            //'cid'=>'required',
        ]);
		if($status){

			$cid = $this->request->getpost('cid');
			$cname = $this->request->getpost('cname');

			$data = array(
				'name'	=> $cname
				);

			$data['category'] = $this->catemodel->update($cid, $data);
			success('admin/category/index', '修改成功');
		} else {
			helper('form');
			echo view('admin/edit_cate.html');
		}
	}


	/**
	 * 删除栏目
	 */
	public function del($cid){
		//$cid = $this->uri->segment(4);
		$this->catemodel->delete($cid);
		success('admin/category/index', '删除成功');
	}

   


}
