<?php
namespace App\Controllers\Admin;

use App\Models\AdminModel;
use App\Models\ModUser;
class Login extends \CodeIgniter\Controller
{
	// public function index(){
    //     $session = \Config\Services::session();
    //     $data['message']=$session->getFlashdata('message');
    //     helper('form');

    //     return view('signin',$data);
    // }
/**
	 * 登陆默认方法
	 * @return [type] [description]
	 */
	public function index(){
		helper('form');
		return view('admin/login');
	}

	public function checkuser(){
        $myrequest = \Config\Services::request();
        $session = \Config\Services::session();
        $myvalues = $this->validate([
            'userName'=>'required',
            'userPwd'=>'required',
        ]);
        if(!$myvalues){//没有通过条件检验
            return $this->index();
        }else{
    /*新建modeuser也可以，为了规范，使用adminmodel，moduser用来学习参考
    $user = new ModUser();*/
        $admin = new AdminModel();
           // helper('text');
           $username = $myrequest->getVar('userName');
           // var_dump($username);
            $data['userPwd'] = $myrequest->getVar('userPwd');
            //$data['u_link'] = random_string('alnum',20);
         
/* 知识点：建设一个moduser模型，也可以直接在control里使用查询构造器
$allUsers = $user->where('username',$username)->findAll();*/
            $allUsers = $admin->check($username);
           // $allUsers = $user->find([1,2,3]);
			//var_dump($allUsers);exit;
            //if(!$allUsers || $allUsers[0]['userpwd'] != md5($data['userPwd'])) error('用户名或者密码不正确');
            if(!$allUsers || $allUsers['userpwd'] != md5($data['userPwd'])) error('用户名或者密码不正确');
            // if(count($allUsers) > 0){
            //     if($data['userPwd'] == $allUsers[0]['userpwd']){
            //            echo '登录成功';
            //     }else{
            //         echo '登录失败';
            //     }
            // }else{

            // }
            $sessionData = array(
                'username'	=> $username,
               // 'uid'		=> $allUsers[0]['uid'],
               'uid'		=> $allUsers['uid'],
                'logintime' => time()
                );
                $session->set($sessionData);
                //var_dump($session->get());
                success('admin/admin/index', '登陆成功');




        }
    }
/**
	 * 退出登陆
	 */
	public function login_out(){
   
		$session = \Config\Services::session();
        $session->destroy();
		success('admin/login/index','退出成功');
	}

}
