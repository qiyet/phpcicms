<?php namespace App\Controllers;
use App\Models\ModUsers;

class User extends BaseController{

    public function index(){
        echo 'I am fine';
    }

    public function register(){
        // echo '注册开始';
        helper('form');
        //$session = \Config\Services::session();
        //$data['message']=$session->getFlashdata('message');
        //return view('signup',$data);
        return view('signup');
    }

    public function newuser(){
        $myvalues = $this->validate([
            'name'=>'required',
            'email'=>'required',
            'password'=>'required',
        ]);
        if(!$myvalues){//没有通过条件检验
            return $this->register();
        }else{//通过条件检验后，取值
            $myrequest = \Config\Services::request();
            $session = \Config\Services::session();
            $users = new ModUsers();
            // echo $myrequest->getVar('name');
            // echo $myrequest->getVar('email');
            // echo $myrequest->getVar('password');
            helper('text');

            $data['u_name'] =  $myrequest->getVar('name');
            $data['u_email'] = $myrequest->getVar('email');
            $data['u_password'] = $myrequest->getVar('password');
            $data['u_password']  = hash('md5',$data['u_password'] );
            $data['u_link'] = random_string('alnum',20);

        
            //这里就看得出u_link是个随机生成的链接。更好的实现是通过session来保存一下，并且在一定时间后自动销毁。
            $message = 'Please activate the account'.anchor('user/activate/'.$data['u_link'],'Activate account here','');
            
            $checkUserExists = $users->where('u_email',$data['u_email'])->findAll();
            if(count($checkUserExists) > 0){
                $session->setFlashdata('message','The email is already used');
                return redirect()->to(site_url('user/newuser'));
                // echo 'The email exists';
            }else{
                $myNewuser  = $users->insert($data);
                if($myNewuser){
                    echo 'make it to  insert';
                    $email = \Config\Services::email();
                    $email->setFrom('guoxingyao@gmail.com','Activate the account');
                    $email->setTo($data['u_email']);
                    $email->setSubject('Activiate your account');
                    $email->setMessage($message);
                    
                    if($email->send())
                    {
                        echo 'Email sent successfully';
                    }else{
                        $session->setFlashdata('message','The email sending fails');
                        return redirect()->to(site_url('user/newuser'));
                    }
                }else{
                    $session->setFlashdata('message','The insertion fails');
                    return redirect()->to(site_url('user/newuser'));
                }
            }
        }
    }

    public function activate($linkhere){
        $user = new ModUsers();
        $checklink = $user->where('u_link', $linkhere)->findAll();
        if(count($checklink) > 0){
            $data['u_status'] = 1;
            $activateUser = $user->update($checklink[0]['u_id'],$data);
            if($activateUser){
                echo 'ok';
            }
            else{
                echo 'failed';
            }
        }else{
            echo 'expired';
        }
    }

    public function signin(){
        $session = \Config\Services::session();
        $data['message']=$session->getFlashdata('message');
        helper('form');

        return view('signin',$data);
    }

    public function checkuser(){

        $myrequest = \Config\Services::request();
        $session = \Config\Services::session();
        $myvalues = $this->validate([
            'email'=>'required',
            'password'=>'required',
        ]);
        if(!$myvalues){//没有通过条件检验
            return $this->signin();
        }else{
            $users = new ModUsers();
            helper('text');

            $data['u_email'] = $myrequest->getVar('email');
            var_dump($data['u_email']);
            $data['u_password'] = $myrequest->getVar('password');
            $data['u_link'] = random_string('alnum',20);
            $allUsers = $users->where('u_email',$data['u_email'])->findAll();
            if(count($allUsers) > 0){
                if($data['u_password'] == $allUsers[0]['u_password']){

                    echo 'login invalid';
                }else{
                    echo 'login valid';
                }
            }else{

            }
        }
    }
}


