<?php

namespace App\Controllers;

class Home extends BaseController
{
	public $category;
	public $title;

	public function __construct(){
		//parent::__construct();

		$this->art = new \App\Models\ArticleModel();
		$this->cate = new \App\Models\CategoryModel();

		$this->category = $this->cate->limit_category(4);
		$this->title = $this->art->title(10);
	}
    public function index()
    {
       // return view('welcome_message');
	   $data = $this->art->check();

		$data['category'] = $this->category;
		//var_dump($data['category']);exit;

		$data['title'] = $this->title;
		//var_dump($data);
		$this->cachePage(1/6);//其中 $n 是缓存更新的时间（单位分钟）,可以使用m/60来精确到秒，例如1/60，则是精确到1秒
		echo view('index/home', $data);

    }

/**
	 * 分类页显示
	 */
	public function category($cid){
		$data['category'] = $this->category;

		$data['title'] = $this->title;

		//$cid=1;

		$data['article'] = $this->art->category_article($cid);
		//var_dump($data);exit;


		echo view('index/category', $data);
	}
/**
	 * 文章阅读页显示
	 */
	public function article($aid){
		//$aid = $this->uri->segment(2);

		$data['category'] = $this->category;

		$data['title'] = $this->title;

		$data['article'] = $this->art->aid_article($aid);

		//p($data);

		 echo view('index/article', $data);
	}




	public function aaa(){
		echo '测试sqlite';
		$db = \Config\Database::connect();
		$DBPrefix = $db->getPrefix();
	echo $DBPrefix;
	$query = $db->query("SELECT * FROM ci_user LIMIT 1;");
	var_dump($query);
	}
//测试qi
    public function method(){
		echo '没错，就是我';
	}


    public function setsession(){
		$mysession = session();
		//var_dump($mysession);
		echo '==============';
		$myarray = [
			'name'=>'yyys',
			'email'=>'yyyfadfs',
			'add'=>'yyysfadga'
		];
		$mysession->set('key',$myarray);
        var_dump($mysession);
	}
	public function getsession(){
		$mysession = session();
		var_dump($mysession->get('key'));
	}
	public function destroysession(){
		$mysession = session();
		$mysession->destroy();
	}


}
