<?php

namespace App\Controllers;

class Test extends BaseController
{
	public $category;
	public $title;

	public function __construct(){
		//parent::__construct();

		$this->art = new \App\Models\ArticleModel();
		$this->cate = new \App\Models\CategoryModel();

		$this->category = $this->cate->limit_category(4);
		$this->title = $this->art->title(10);
	}
    public function index()
    {
       // return view('welcome_message');
	   $data = $this->art->check();

		$data['category'] = $this->category;
		//var_dump($data['category']);exit;

		$data['title'] = $this->title;
		//var_dump($data);
		$this->cachePage(1/6);//其中 $n 是缓存更新的时间（单位分钟）,可以使用m/60来精确到秒，例如1/60，则是精确到1秒
		echo view('index/test', $data);

    }
	
	// 上传图片
    public function uploads() {
        // 接收数据
        $file = $this->request->getFiles('imageData');
        // 判断是否上传成功
        if (!$file['imageData']->isValid()) {
            return json_encode(['status' => 0,'message' => '文件上传失败']);
        }
        // 获取文件扩展名
        $ext = $file['imageData']->getClientExtension();

        // 判断文件类型是否允许
        if (! in_array($ext,['jpg','png','gif'])) {
            return json_encode(['status' => 0,'message' => '文件类型不允许']);
        }

        // 为避免一个文件夹中的文件过多和文件名重复,所以需要设置上传文件夹和文件名
       // $fileName = $this->setFilePath(_UPLOADS_.'/'.date('Y_m_d'),$ext);
	   $newName = $file['imageData']->getRandomName(); 

        // 上传文件并判断
        if ($file['imageData']->move('up', $newName)) {
			$fileName='up/'.$newName;
            return json_encode([
                'status'  => 1,
                'message' => '文件上传成功',
                'img'     => $fileName
            ]);
        }
    }

    /**
     * 设置文件路径和文件名称
     * @param string $path 文件要上传的目标文件夹
     * @param string $ext  文件后缀名
     * @return strint      返回完整的路径+文件名称
     */
    public function setFilePath ($path,$ext) {
        // 修正路径和文件后缀名
        $path = rtrim($path,'/').'/';
        $ext  = '.'.trim($ext,'.');
        // 设置文件名
        if (! file_exists($path)) {
            @mkdir($path);
        }
        // 设置文件名
        do{
            $fileName = time().mt_rand();
        }while( file_exists($path.$fileName.$ext) );
        return $path.$fileName.$ext;
    }

	public function aaa(){
		echo '测试sqlite';
		$db = \Config\Database::connect();
		$DBPrefix = $db->getPrefix();
	echo $DBPrefix;
	$query = $db->query("SELECT * FROM ci_user LIMIT 1;");
	var_dump($query);
	}
//测试qi
    public function method(){
		echo '没错，就是我';
	}


    public function setsession(){
		$mysession = session();
		//var_dump($mysession);
		echo '==============';
		$myarray = [
			'name'=>'yyys',
			'email'=>'yyyfadfs',
			'add'=>'yyysfadga'
		];
		$mysession->set('key',$myarray);
        var_dump($mysession);
	}
	public function getsession(){
		$mysession = session();
		var_dump($mysession->get('key'));
	}
	public function destroysession(){
		$mysession = session();
		$mysession->destroy();
	}


}
