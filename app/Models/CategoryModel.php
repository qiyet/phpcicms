<?php
namespace App\Models;
		/*** 栏目管理模型*/
class CategoryModel extends \CodeIgniter\Model
    {
		 protected $table = 'category';//指定的表
		 // 查询返回的数据类型 array数组 object对象
		 protected $returnType = 'array';

		 // 可写字段名称数组     当添加/更新时, 自动将不在数组中的字段过滤掉
		 protected $allowedFields = [
			 'id',
			 'pid',
			 'name',
		 ];
		 // 是否自动添加写入时间 false否 true是  当添加/更新时, 自动将操作时间写入数据表中
		 protected $useTimestamps = true;

		 // 创建时间字段列名
		 protected $createdField  = 'create_time';
	 
		 // 更新时间字段列名
		 protected $updatedField  = 'update_time';
	 
		 // 软删除(逻辑删除)时间字段列名
		 protected $deletedField  = 'delete_time';
	 
		 // 时间格式  datetime, date, int
		 protected $dateFormat = 'int';
	 
		 // 是否使用软删除(逻辑删除) false否 true是
		 protected $useSoftDeletes = false;
        /**
	 * 添加
	 */
	public function add($data){
		$this->insert($data);
	}

	/**
	 * 查看所有栏目
	 */
	public function check(){
		$data = $this->findAll();//get()结果是对象，findall 是数组
		return $data;
	}


	/**
	 * 查询指定的一条对应栏目
	 */
	public function check_cate($cid){
		$data = $this->where(array('id'=>$cid))->first();
		return $data;
	}
    	
	/**
	 * 调取导航栏栏目
	 */
	public function limit_category($limit){
		$data = $this->findAll($limit);
		return $data;
	}

    }