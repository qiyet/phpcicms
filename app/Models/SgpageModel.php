<?php
namespace App\Models;
//文章管理模型
class SgpageModel extends \CodeIgniter\Model
    {
           
		 protected $table = 'sgpage';//指定的表
		 // 查询返回的数据类型 array数组 object对象
		 protected $returnType = 'array';

		 // 可写字段名称数组     当添加/更新时, 自动将不在数组中的字段过滤掉
		 protected $allowedFields = [
			 'id',
			 'cid',
			 'title',
			 'keyword',
			 'description',
			 'image',
			 'content',
			 'isdel',
		 ];
		 // 是否自动添加写入时间 false否 true是  当添加/更新时, 自动将操作时间写入数据表中
		 protected $useTimestamps = true;

		 // 创建时间字段列名
		 protected $createdField  = 'create_time';
	 
		 // 更新时间字段列名
		 protected $updatedField  = 'update_time';
	 
		 // 软删除(逻辑删除)时间字段列名
		 protected $deletedField  = 'delete_time';
	 
		 // 时间格式  datetime, date, int
		 protected $dateFormat = 'int';
	 
		 // 是否使用软删除(逻辑删除) false否 true是
		 protected $useSoftDeletes = false;
	
/**
	 * 发表文章
	 */
	public function add($data){
		$this->insert( $data);
	}

	/**
	 * 查看文章
	 */
	public function article_category(){
		$data = $this->select('id,title,name,creat_time')->from('article')->join('category', 'article.id=category.cid')->orderBy('id', 'asc')->findAll();
		return $data;
	}


	/**
	 * 首页查询文章
	 */
	public function check(){
		$data['art'] = $this->select('id,image,title,description')->orderBy('update_time', 'desc')->where(['isdel' =>0])->findAll();

		$data['hot'] = $this->select('id,image,title,description')->orderBy('update_time', 'desc')->where(array('type'=>1))->findAll();

		return $data;
	}

	/**
	 * 右侧文章标题调取
	 */
	public function title($limit){
		$data = $this->select('title,id')->orderBy('update_time', 'desc')->limit($limit)->findAll();
		return $data;
	}

	/**
	 * 通过栏目调取文章
	 */
	public function category_article($cid){
		$data = $this->select('id,image,title,description')->where( array('id'=>$cid))->findAll();
		return $data;
	}


	/**
	 * 通过aid 调取文章
	 */
	
	public function aid_article($aid){
		$data = $this->join('category', 'article.cid=category.id')->where(array('article.id'=>$aid))->first();
		return $data;
	}


    }