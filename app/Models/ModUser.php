<?php
namespace App\Models;
use CodeIgniter\Model;
class ModUser extends Model 
{
    //by qi
    protected $DBGroup = 'default';
    protected $table = 'user';
    protected $primaryKey = 'uid';
   protected $returnType = 'array';
    //protected $useTimestamps = true;
    protected $allowedFields = ['uid','username','userpwd'];
   // protected $createdField = 'u_date';
    //protected $updatedField = 'u_updated';
}
